/*
Copyright (c) 2015-2016, Tom Albans <albans.thomas@gmail.com>

This file is part of Trippy.

Trippy is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Trippy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Trippy.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "reminders.h"

int single_shot(int argc, char **argv) {

	// all 3 parameters were sent to the program, send them all in one big bulk
	int bytecount = 0;
	string data = "";
	if (argc == 4) {
		// there are 3 arguments, one for each element of the reminder
		// combine and separate the data using pipeline '|' characters
		for (unsigned int x = 1; x < argc; x++) {
			data += argv[x];
			if (x != (argc - 1)) {
				data += "|";
			}
		}
	} else if (argc == 2) {
		// all the data that has been sent has already been separated by pipeline '|' characters
		data = argv[1];
	} else {
		// an invalid amount of arguments were passed
		cout << "An incorrect amount of arguments were passed" << endl;
		return 1;
	}
	// add the ADD REMINDER instruction to the start
	data = "A" + data + "\0";

	// send the data in one bulk to the server
	if (send_data(data, true) != 0) {
		return 1;
	}
	cout << "Reminder successfully added." << endl;
	return 0;
}

int send_data(string data, bool verbose) {

	int sockfd = 0;
	struct sockaddr_in serv_addr;

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		if (verbose) {
			cout << "\n Error: Could not create socket \n";
		}
		return 1;
	}

	get_ip(address);

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(rem_port);
	serv_addr.sin_addr.s_addr = inet_addr(ip);

	if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
		if (verbose) {
			cout << "\n Error: Couldn't connect to server \n";
		}
		return 1;
	}

	int bytecount = 0;
	data += "\0";

	// send the data in one bulk to the server
	if ((bytecount = send(sockfd, data.c_str(), data.length(), 0)) == -1) {
		if (verbose) {
			cout << "The data couldn't be sent." << endl;
		}
		return 1;
	}
	close(sockfd);
	return 0;
}

string receive_data(bool verbose) {

	string data;
	int sockfd = 0;
	struct sockaddr_in serv_addr;

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		if (verbose) {
			cout << "\n Error: Could not create socket \n";
		}
		return "";
	}

	get_ip(address);

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(rem_port);
	serv_addr.sin_addr.s_addr = inet_addr(ip);

	if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
		if (verbose) {
			cout << "\n Error: Couldn't connect to server \n";
		}
		return "";
	}

	char *recBuf = new char[4097];
	memset(recBuf, 0, 4097);
	int bytecount = 0;

	if ((bytecount = recv(sockfd, recBuf, 4096, 0)) > 0) {
		data = recBuf;
		memset(recBuf, 0, 4097); // clear the receiving buffer
	} else if (verbose) {
		cout << "The data could not be retrieved." << endl;
		delete[] recBuf;
		return "";
	}

	delete[] recBuf;
	close(sockfd);
	return data;
}

void synchronise() {
	// this function is run in a separate thread that synchronises any reminders left in the queue and retrieves any new reminders
	while (true) {
		pause_sync();

		mtxSync.lock();
		// check if there are any reminders in the queue, if there is then attempt to synchronise them with the server
		while (!rem_queue.empty()) {
			// there are reminders in the queue so attmept to synchronise them all
			// communicate with the server and send the data across
			string data = rem_queue.front();
			// attempt to send the instruction to the server
			if (send_data(data, false)) {
				// the reminder couldn't be sent to the server so break from the loop
				break;
			}
			// the reminder was successfully sent to the server, remove that reminder from the queue
			rem_queue.pop();
		}

		if (!rem_queue.empty()) {
			// some reminders weren't synchronised therefore connection to the server failed, so start the loop again
			continue;
		}

		// now attempt to synchronise with the server
		// all reminders received from the server are all the current and updated reminders
		int bytecount = 0;
		int sockfd = 0;
		struct sockaddr_in serv_addr;

		if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
			cout << "\n Error: Could not create socket \n";
			close(sockfd);
			continue;
		}

		get_ip(address);

		serv_addr.sin_family = AF_INET;
		serv_addr.sin_port = htons(rem_port);
		serv_addr.sin_addr.s_addr = inet_addr(ip);
		mtxSync.unlock();

		if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
			cout << "\n Error: Couldn't connect to server \n";
			close(sockfd);
			continue;
		}

		if ((bytecount = send(sockfd, "SYNC", 4, 0)) == -1) {
			cout << "The data couldn't be sent." << endl;
			close(sockfd);
			continue;
		}

		// the SYNC command was successfully sent to the server, so retrieve all its data
		string data = "";
		mtxSync.lock();
		reminders.clear();
		mtxSync.unlock();
		gdk_threads_add_idle(rem_clear_tree, 0);
		char *recBuf = new char[4097];
		memset(recBuf, 0, 4097);

		while (true) {
			while ((bytecount = recv(sockfd, recBuf, 4096, 0)) > 0) {
				data += recBuf;
				memset(recBuf, 0, 4097); // clear the receiving buffer
			}

			if (data == "") {
				break;
			}

			while (data != "") {

				reminder temp;
				// all the reminders have been sent all at once
				// each reminder is split up into segments and handled individually
				string segment;
				int seg_i = data.find((char)30);
				if (seg_i >= 0 && seg_i < data.length()) {
					segment = data.substr(0, seg_i);
					data.erase(0, seg_i + 1);
				}

				int i = segment.find("|");
				temp.title = segment.substr(0, i);
				segment.erase(0, i + 1);

				if (segment.substr(0, 4) != "null") {
					i = segment.find("/");
					temp.date.tm_mday = to_int(segment.substr(0, i));
					segment.erase(0, i + 1);
					i = segment.find("/");
					temp.date.tm_mon = to_int(segment.substr(0, i)) - 1;
					segment.erase(0, i + 1);
					temp.date.tm_year = to_int(segment.substr(0, 4)) - 1900;
					segment.erase(0, segment.find(" ") + 1);

					i = segment.find(":", i + 1);
					temp.date.tm_hour = to_int(segment.substr(0, i));
					segment.erase(0, i + 1);
					i = segment.find("|");
					temp.date.tm_min = to_int(segment.substr(0, i));
					segment.erase(0, i + 1);
				} else {
					segment.erase(0, segment.find("|") + 1);
				}

				if (segment == "null" || segment == "null\n") {
					temp.notes = "";
				} else {
					temp.notes = segment;
				}
				mtxSync.lock();
				reminders.push_back(temp);
				mtxSync.unlock();
			}
		}
		close(sockfd);
		delete[] recBuf;

		// add all the items into the listbox all at once obtaining only one thread lock
		gdk_threads_add_idle(rem_add_items, 0);
	}
}

int rem_clear_tree(gpointer data) {
	gtk_tree_store_clear(GTK_TREE_STORE(listboxstore));
	return G_SOURCE_REMOVE;
}

int rem_add_items(gpointer data) {
	for (unsigned int x = 0; x < reminders.size(); x++) {
		add_list_item(reminders[x].title.c_str());
	}
	return G_SOURCE_REMOVE;
}

void time_passed() {
	// this function runs in a separate thread and checks if any reminder times have passed
	while (true) {
		pause(2000); // check if any reminders have passed every 2 seconds
					 // first get the current time and the reminder time to compare it to
		time_t current = time(0);
		for (unsigned int x = 0; x < reminders.size(); x++) {
			if (reminders[x].date.tm_year <= 0) {
				// either no reminder was set or the user was already reminded of this reminder, skip it
				continue;
			}
			time_t comp = mktime(&reminders[x].date);
			double diff = difftime(current, comp);
			// if the result is positive then the reminder time has passed
			// the current time in seconds is greater than the reminder time in seconds
			if (diff > 0) {
				// the reminder has passed
				gdk_threads_add_idle(reminder_passed, (void*)(reminders[x].title.c_str()));
				mtxSync.lock();
				reminders[x].date = tm();
				mtxSync.unlock();
			}
		}
	}
}

int reminder_passed(gpointer data) {
	message_box((char*)data, "Reminder!", GTK_BUTTONS_OK, GTK_MESSAGE_INFO);
	return G_SOURCE_REMOVE;
}

void read_config() {
    // this function reads the data in the configuration file and applies it
    Config conf;

    try {
        // attempt to read in the config file
        conf.readFile("config.cfg");

        string server = conf.lookup("server");
        int port = conf.lookup("port");
        int update = conf.lookup("update");
        bool backgr = conf.lookup("background");

        // the data was successfully read so apply it
        if (!server.empty()) {
            address = server;
        }
        if (port > 0) {
            rem_port = port;
        }
        if (update > 0) {
            interval = update;
        }
        background = backgr;
    } catch (const ParseException &ex) {
        message_box("There was a configuration parse error!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
    } catch (std::exception) {
        return;
    }
}

void write_config() {
    // this function writes the data into the configuration file
    mtxSync.lock();

    ofstream write("config.cfg");
    if (write.fail()) {
        message_box("The configuration file could not be written!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
        mtxSync.unlock();
        return;
    }
    write << "server = \"" << address << "\";\n";
    write << "port = " << rem_port << ";\n";
    write << "update = " << interval << ";\n";
    write << "background = " << ((background) ? "true;" : "false;");
    write.close();

    mtxSync.unlock();
}

void pause(int duration) {
	// sleep for the designated amount of time in milliseconds
	std::this_thread::sleep_for(std::chrono::milliseconds(duration));
	return;
}

void pause_sync() {
	// sleep for the designated amount of time in milliseconds
	unsigned int x = 0;
	while (true) {
		//mtxSync.lock();
		if (x >= interval || sync_rem) {
			// the synchronise interval was reached or a synchronisation was requested
			sync_rem = false;
			return;
		}
		//mtxSync.unlock();
		std::this_thread::sleep_for(std::chrono::seconds(1));
		x++;
	}
	return;
}
