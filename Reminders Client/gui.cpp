/*
Copyright (c) 2015-2016, Tom Albans <albans.thomas@gmail.com>

This file is part of Trippy.

Trippy is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Trippy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Trippy.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "reminders.h"

GtkStatusIcon *trayicon;
GtkWidget *window;
GtkWidget *fixed;

GtkWidget *menubar;
GtkWidget *filemenu;
GtkWidget *menuFile;
GtkWidget *menuSync;
GtkWidget *menuQuit;
GtkWidget *menuHelp;
GtkWidget *menuAbout;

GtkWidget *notebook;
GtkWidget *noteRemFixed;
GtkWidget *noteRemFrame;
GtkWidget *noteReminder;
GtkWidget *noteOptFixed;
GtkWidget *noteOptFrame;
GtkWidget *noteOptions;

GtkWidget *listbox;
GtkTreeStore *listboxstore;
GtkWidget *lstScrollWindow;
GtkWidget *noteRembtnEdit;
GtkWidget *noteRembtnAdd;
GtkWidget *noteRemlblReminder;
GtkWidget *noteRemtxtReminder;
GtkWidget *noteRemchkDate;
GtkWidget *noteRemcboDD;
GtkWidget *noteRemcboMM;
GtkWidget *noteRemcboYYYY;
GtkWidget *noteRemspinHH;
GtkWidget *noteRemspinMM;
GtkWidget *noteRemlblHourMin;
GtkWidget *noteRemlblNotes;
GtkWidget *noteRemscrollNotes;
GtkWidget *noteRemtxtNotes;


GtkWidget *noteOptlblServer;
GtkWidget *noteOpttxtServer;
GtkWidget *noteOptbtnServer;
GtkWidget *noteOptlblIP;
GtkWidget *noteOptlblInterval;
GtkWidget *noteOptspinInterval;
GtkWidget *noteOptbtnInterval;


const unsigned int width = 640;
const unsigned int height = 430;
unsigned int rem_port = 5050;
unsigned int interval = 60 * 5; // a default of 5 minutes for the sync interval
bool background = false;
string address = "localhost";
int edit_rem;
mutex mtxSync;
mutex mtxRem;
bool editing = false;
bool sync_rem = true;
char ip[40];

vector<reminder> reminders; // contains all the reminders
queue<string> rem_queue; // contains all the reminders to be synchronised with the server

int main(int argc, char **argv) {

#ifdef _WIN32
	WSADATA wsa_data;
	if (WSAStartup(MAKEWORD(1, 1), &wsa_data) != 0) {
		cout << "The Windows socket library could not be loaded!" << endl;
		return 1;
	}

	if (argc >= 2) {
		// the client has been invoked with arguments so add the reminder that has been passed and then close
		int res = single_shot(argc, argv);
		WSACleanup();
		return res;
	}
#ifndef _DEBUG
	{
		HWND var = GetConsoleWindow();
		ShowWindow(var, SW_HIDE);
	}
#endif
#else
	if (argc >= 2) {
		// the client has been invoked with arguments so add the reminder that has been passed and then close
		return single_shot(argc, argv);
	}
#endif

    // get the application lock
	if (!get_lock()) {
        // a reminders client process is already running
        return 0;
	}

	// initialise and create the GUI
	gtk_init(&argc, &argv);

	// read the configuration file and apply the settings
    read_config();

	// create and show the main window
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_default_size(GTK_WINDOW(window), width, height);
	gtk_widget_set_size_request(GTK_WIDGET(window), width, height);
	gtk_window_set_title(GTK_WINDOW(window), "Reminders");
	gtk_window_set_resizable(GTK_WINDOW(window), FALSE);

	// set the reminders icon for the window
	GdkPixbuf *pixbuf;
	GError *error = NULL;
	pixbuf = gdk_pixbuf_new_from_file("reminders-icon.png", &error);
	if (pixbuf) {
		gtk_window_set_icon(GTK_WINDOW(window), pixbuf);
	}
	else {
		// the image couldn't be applied to the window so apply a GTK stock icon
		gtk_window_set_icon_name(GTK_WINDOW(window), GTK_STOCK_APPLY);
	}

	// create the system tray icon
	trayicon = gtk_status_icon_new_from_file("reminders-icon.png");
	if (gtk_status_icon_get_pixbuf(trayicon) == 0) {
		// the image couldn't be set, so set the icon to a GTK stock icon
		gtk_status_icon_set_from_stock(trayicon, GTK_STOCK_APPLY);
	}
	gtk_status_icon_set_tooltip_text(trayicon, "Reminders");
	gtk_status_icon_set_visible(trayicon, TRUE);

	// create the window widgets

	fixed = gtk_fixed_new();
	gtk_container_add(GTK_CONTAINER(window), fixed);

	// create the notebook with labels and containers
	noteRemFixed = gtk_fixed_new();
	noteRemFrame = gtk_frame_new("Reminders");
	gtk_widget_set_size_request(noteRemFrame, 630, 390);
	noteOptFixed = gtk_fixed_new();
	noteOptFrame = gtk_frame_new("Options");
	gtk_widget_set_size_request(noteOptFrame, 630, 390);
	notebook = gtk_notebook_new();
	noteReminder = gtk_label_new("Reminders");
	noteOptions = gtk_label_new("Options");
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), noteRemFixed, noteReminder);
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), noteOptFixed, noteOptions);
	gtk_fixed_put(GTK_FIXED(noteRemFixed), noteRemFrame, 0, 0);
	gtk_fixed_put(GTK_FIXED(noteOptFixed), noteOptFrame, 0, 0);
	gtk_fixed_put(GTK_FIXED(fixed), notebook, 0, 25);


	// create the widgets for the reminders page

	// create the menu bar and populate it with items
	menubar = gtk_menu_bar_new();
	menuFile = gtk_menu_item_new_with_label("File");
	menuSync = gtk_menu_item_new_with_label("Sync...");
	menuQuit = gtk_menu_item_new_with_label("Quit");
	menuHelp = gtk_menu_item_new_with_label("Help");
	menuAbout = gtk_menu_item_new_with_label("About Reminders");

	filemenu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuFile), filemenu); // "File"
	gtk_menu_shell_append(GTK_MENU_SHELL(filemenu), menuSync); // "Synchronise"
	gtk_menu_shell_append(GTK_MENU_SHELL(filemenu), gtk_menu_item_new()); // separator
	gtk_menu_shell_append(GTK_MENU_SHELL(filemenu), menuQuit); // "Quit"
	gtk_menu_shell_append(GTK_MENU_SHELL(menubar), menuFile); // add the menu into the bar

	filemenu = gtk_menu_new();
	gtk_menu_item_set_submenu(GTK_MENU_ITEM(menuHelp), filemenu); // "Help"
	gtk_menu_shell_append(GTK_MENU_SHELL(filemenu), menuAbout); // "About..."
	gtk_menu_shell_append(GTK_MENU_SHELL(menubar), menuHelp); // add the menu into the bar
	gtk_container_add(GTK_CONTAINER(fixed), menubar);

	// create the listbox
	listboxstore = gtk_tree_store_new(1, G_TYPE_STRING);
	listbox = gtk_tree_view_new_with_model(GTK_TREE_MODEL(listboxstore));
	g_object_unref(listboxstore);
	GtkCellRenderer *renderer = gtk_cell_renderer_text_new();
	GtkTreeViewColumn *col = gtk_tree_view_column_new_with_attributes("Reminders", renderer, "text", 0, 0);
	gtk_tree_view_append_column(GTK_TREE_VIEW(listbox), col);
	g_object_set(listbox, "activate-on-single-click", TRUE, 0);

	lstScrollWindow = gtk_scrolled_window_new(0, 0);
	gtk_container_add(GTK_CONTAINER(lstScrollWindow), listbox);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(lstScrollWindow), GTK_SHADOW_ETCHED_IN);

	gtk_widget_set_size_request(lstScrollWindow, 280, 360);
	gtk_fixed_put(GTK_FIXED(noteRemFixed), lstScrollWindow, 12, 25);


	// create the widgets for adding/viewing reminders
	noteRembtnEdit = gtk_button_new_with_label("Edit");
	gtk_widget_set_size_request(noteRembtnEdit, 80, 40);
	gtk_fixed_put(GTK_FIXED(noteRemFixed), noteRembtnEdit, 300, 30);

	noteRembtnAdd = gtk_button_new_with_label("Add reminder");
	gtk_widget_set_size_request(noteRembtnAdd, 120, 40);
	gtk_fixed_put(GTK_FIXED(noteRemFixed), noteRembtnAdd, 500, 30);

	noteRemlblReminder = gtk_label_new("Reminder:");
	gtk_fixed_put(GTK_FIXED(noteRemFixed), noteRemlblReminder, 300, 85);

	noteRemtxtReminder = gtk_entry_new();
	gtk_widget_set_size_request(noteRemtxtReminder, 255, 20);
	gtk_fixed_put(GTK_FIXED(noteRemFixed), noteRemtxtReminder, 365, 81);

	noteRemchkDate = gtk_check_button_new_with_label("Date/Time");
	gtk_fixed_put(GTK_FIXED(noteRemFixed), noteRemchkDate, 300, 110);

	noteRemcboDD = gtk_combo_box_text_new();
	for (unsigned int x = 1; x <= 31; x++) {
		gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(noteRemcboDD), std::to_string(x).c_str());
	}
	gtk_widget_set_sensitive(noteRemcboDD, false);
	gtk_fixed_put(GTK_FIXED(noteRemFixed), noteRemcboDD, 394, 110);

	noteRemcboMM = gtk_combo_box_text_new();
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(noteRemcboMM), "January");
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(noteRemcboMM), "February");
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(noteRemcboMM), "March");
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(noteRemcboMM), "April");
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(noteRemcboMM), "May");
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(noteRemcboMM), "June");
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(noteRemcboMM), "July");
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(noteRemcboMM), "August");
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(noteRemcboMM), "September");
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(noteRemcboMM), "October");
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(noteRemcboMM), "November");
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(noteRemcboMM), "December");
	gtk_widget_set_sensitive(noteRemcboMM, false);
	gtk_fixed_put(GTK_FIXED(noteRemFixed), noteRemcboMM, 445, 110);

	time_t t = time(0);
	tm *curr = localtime(&t);
	noteRemcboYYYY = gtk_combo_box_text_new();
	for (unsigned int x = (curr->tm_year + 1900); x <= (curr->tm_year + 1910); x++) {
		gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(noteRemcboYYYY), std::to_string(x).c_str());
	}
	gtk_widget_set_sensitive(noteRemcboYYYY, false);
	gtk_fixed_put(GTK_FIXED(noteRemFixed), noteRemcboYYYY, 550, 110);

	noteRemlblHourMin = gtk_label_new("Hour/Minutes:");
	gtk_widget_set_sensitive(noteRemlblHourMin, false);
	gtk_fixed_put(GTK_FIXED(noteRemFixed), noteRemlblHourMin, 305, 139);

	noteRemspinHH = gtk_spin_button_new_with_range(0, 23, 1);
	gtk_widget_set_sensitive(noteRemspinHH, false);
	gtk_fixed_put(GTK_FIXED(noteRemFixed), noteRemspinHH, 390, 135);

	noteRemspinMM = gtk_spin_button_new_with_range(0, 59, 1);
	gtk_widget_set_sensitive(noteRemspinMM, false);
	gtk_fixed_put(GTK_FIXED(noteRemFixed), noteRemspinMM, 495, 135);

	noteRemlblNotes = gtk_label_new("Notes");
	gtk_fixed_put(GTK_FIXED(noteRemFixed), noteRemlblNotes, 302, 169);

	noteRemtxtNotes = gtk_text_view_new();
	gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(noteRemtxtNotes), GTK_WRAP_WORD_CHAR);
	noteRemscrollNotes = gtk_scrolled_window_new(0, 0);
	gtk_container_add(GTK_CONTAINER(noteRemscrollNotes), noteRemtxtNotes);
	gtk_widget_set_size_request(noteRemscrollNotes, 320, 200);
	gtk_fixed_put(GTK_FIXED(noteRemFixed), noteRemscrollNotes, 300, 185);

	// set the default values for the combo boxes so segmentation faults can be avoided
	gtk_combo_box_set_active(GTK_COMBO_BOX(noteRemcboDD), curr->tm_mday - 1);
	gtk_combo_box_set_active(GTK_COMBO_BOX(noteRemcboMM), curr->tm_mon);
	gtk_combo_box_set_active(GTK_COMBO_BOX(noteRemcboYYYY), 0);


	// create the widgets for the options page
	noteOptlblServer = gtk_label_new("Server address:");
	gtk_fixed_put(GTK_FIXED(noteOptFixed), noteOptlblServer, 20, 50);

	noteOpttxtServer = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(noteOpttxtServer), address.c_str());
	gtk_entry_set_placeholder_text(GTK_ENTRY(noteOpttxtServer), "Enter server URL");
	gtk_widget_set_size_request(noteOpttxtServer, 280, 20);
	gtk_fixed_put(GTK_FIXED(noteOptFixed), noteOpttxtServer, 120, 47);

	noteOptbtnServer = gtk_button_new_with_label("Change");
	gtk_widget_set_size_request(noteOptbtnServer, 80, 40);
	gtk_fixed_put(GTK_FIXED(noteOptFixed), noteOptbtnServer, 400, 38);

	get_ip(address);
	noteOptlblIP = gtk_label_new(string(string("IP Address:  " )+ ip).c_str());
	gtk_fixed_put(GTK_FIXED(noteOptFixed), noteOptlblIP, 124, 74);

	noteOptlblInterval = gtk_label_new("Sync interval (seconds):");
	gtk_fixed_put(GTK_FIXED(noteOptFixed), noteOptlblInterval, 33, 150);

	noteOptspinInterval = gtk_spin_button_new_with_range(1, (60 * 60 * 24), 1);
	gtk_spin_button_set_value(GTK_SPIN_BUTTON(noteOptspinInterval), interval);
	gtk_fixed_put(GTK_FIXED(noteOptFixed), noteOptspinInterval, 190, 146);

	noteOptbtnInterval = gtk_button_new_with_label("Set");
	gtk_widget_set_size_request(noteOptbtnInterval, 80, 40);
	gtk_fixed_put(GTK_FIXED(noteOptFixed), noteOptbtnInterval, 305, 136);



	// connect signals to their appropriate callback functions
	g_signal_connect(G_OBJECT(trayicon), "activate", G_CALLBACK(tray_click), 0);
	g_signal_connect(G_OBJECT(trayicon), "popup-menu", G_CALLBACK(tray_right_click), 0);

	g_signal_connect(G_OBJECT(listbox), "row-activated", G_CALLBACK(reminder_selected), 0);
	g_signal_connect(G_OBJECT(listbox), "key-release-event", G_CALLBACK(delete_reminder), 0);
	g_signal_connect(G_OBJECT(noteRembtnEdit), "clicked", G_CALLBACK(edit_reminder), 0);
	g_signal_connect(G_OBJECT(noteRembtnAdd), "clicked", G_CALLBACK(add_reminder), 0);
	g_signal_connect(G_OBJECT(noteRemtxtReminder), "activate", G_CALLBACK(text_entry_activate), 0);
	g_signal_connect(G_OBJECT(noteRemchkDate), "clicked", G_CALLBACK(toggle_date_time), 0);
	g_signal_connect(G_OBJECT(noteRemcboMM), "changed", G_CALLBACK(date_changed), 0);
	g_signal_connect(G_OBJECT(noteRemcboYYYY), "changed", G_CALLBACK(date_changed), 0);

	g_signal_connect(G_OBJECT(noteOptbtnServer), "clicked", G_CALLBACK(set_server), 0);
	g_signal_connect(G_OBJECT(noteOpttxtServer), "activate", G_CALLBACK(set_server), 0);
	g_signal_connect(G_OBJECT(noteOptbtnInterval), "clicked", G_CALLBACK(set_interval), 0);

	g_signal_connect(G_OBJECT(menuSync), "activate", G_CALLBACK(sync_data), 0);
	g_signal_connect(G_OBJECT(menuQuit), "activate", G_CALLBACK(exit_client), (void*)"ask");
	g_signal_connect(G_OBJECT(menuAbout), "activate", G_CALLBACK(about_info), 0);

	g_signal_connect(G_OBJECT(window), "delete-event", G_CALLBACK(exit_client), 0);

	gtk_widget_show_all(window);


	// create and start the thread for reminder queue synchronisation and retrieval
	thread thrSync(synchronise);
	thrSync.detach();

	thread thrTime(time_passed);
	thrTime.detach();

	gtk_main();
	// when the program quits and the window(s) close the code continues from here
#ifdef _WIN32
	WSACleanup();
#endif
	mtxSync.lock(); // this ensures that the client isn't sending or receiving data from the server
	mtxSync.unlock();
	return 0;
}

void sync_data(GtkWidget *widget) {
	// this function is called when the synchronisation button in the menubar is clicked
	if (message_box(string("Synchronise with the server at: " + address + "?").c_str(), "Confirm", GTK_BUTTONS_YES_NO, GTK_MESSAGE_QUESTION) == GTK_RESPONSE_YES) {
		// synchronise with the server
		sync_rem = true;
	}
}

void toggle_date_time(GtkWidget *widget) {
	// this is called when the checkbox for the date and time is clicked
	// toggle the 'sensitiveness' of the widgets depending on its state
	int state = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(noteRemchkDate));
	gtk_widget_set_sensitive(noteRemcboDD, state);
	gtk_widget_set_sensitive(noteRemcboMM, state);
	gtk_widget_set_sensitive(noteRemcboYYYY, state);
	gtk_widget_set_sensitive(noteRemlblHourMin, state);
	gtk_widget_set_sensitive(noteRemspinHH, state);
	gtk_widget_set_sensitive(noteRemspinMM, state);
}

void date_changed(GtkWidget *widget) {
    // this function is called when the month is changed for the date/time
    // this function will update the days combo box so that no invalid dates are selected
    time_t t = time(0);
	tm *curr = localtime(&t);

	int days = 0;
	int year = gtk_combo_box_get_active(GTK_COMBO_BOX(noteRemcboYYYY)) + curr->tm_year;
	int sel_mon = gtk_combo_box_get_active(GTK_COMBO_BOX(noteRemcboMM));
	int old_sel = gtk_combo_box_get_active(GTK_COMBO_BOX(noteRemcboDD));

    // check which month it is
    switch (sel_mon + 1) {
    case 1:
        // january
        days = 31;
        break;
    case 2:
        // february, check if leap year
        if (year % 4 == 0) {
            // leap year
            days = 29;
        } else {
            // not a leap year
            days = 28;
        }
        break;
    case 3:
        // march
        days = 31;
        break;
    case 4:
        // april
        days = 30;
        break;
    case 5:
        // may
        days = 31;
        break;
    case 6:
        // june
        days = 30;
        break;
    case 7:
        // july
        days = 31;
        break;
    case 8:
        // august
        days = 31;
        break;
    case 9:
        // september
        days = 30;
        break;
    case 10:
        // october
        days = 31;
        break;
    case 11:
        // november
        days = 30;
        break;
    case 12:
        // december
        days = 31;
        break;
    }

    // add the days into the combo box
    gtk_combo_box_text_remove_all(GTK_COMBO_BOX_TEXT(noteRemcboDD));
    for (unsigned int x = 1; x <= days; x++) {
		gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(noteRemcboDD), std::to_string(x).c_str());
	}

	// set the old selection day if it is still valid
	if (old_sel < days) {
        gtk_combo_box_set_active(GTK_COMBO_BOX(noteRemcboDD), old_sel);
	} else {
        gtk_combo_box_set_active(GTK_COMBO_BOX(noteRemcboDD), 0);
	}
}

void set_interval(GtkWidget *widget) {
	// this function is called when the button is clicked in the options page to set the new sync interval
	mtxSync.lock();
	interval = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(noteOptspinInterval));
	mtxSync.unlock();
	message_box("The new synchronisation interval has been set.", "Interval set", GTK_BUTTONS_OK, GTK_MESSAGE_INFO);
	write_config();
}

void tray_click(GtkWidget *widget) {
	// this is called when the tray icon is clicked on
	// this function will bring the window to the front of the screen
	// it does this whether the window is minimised, hidden or underneath another window
	gtk_window_present(GTK_WINDOW(window));
}

void tray_right_click(GtkWidget *widget) {
	// this function is called when the status icon is right-clicked
	// this function will, instead of showing a popup menu, ask the user with a message box whether they want to quit
	if (message_box("Do you wish to quit Reminders?", "Quit?", GTK_BUTTONS_YES_NO, GTK_MESSAGE_QUESTION) == GTK_RESPONSE_YES) {
		// they want to quit the program so quit the GTK loop
		gtk_main_quit();
	}
}

void text_entry_activate(GtkWidget *widget) {
	// this function is called when the entry box for the reminder name is activated
	// depending on whether a current reminder is being edited or not it either adds the reminder or edits the reminder
	if (editing) {
		// a reminder is currently being edited so call the edit reminder function
		edit_reminder(0);
	} else {
		// no reminder is being edited so attempt to add the reminder
		add_reminder(0);
	}
}

void add_reminder(GtkWidget *widget) {
	// this function is called when the user wants to add a reminder
	if (editing) {
		message_box("You are currently editing a reminder!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
		return;
	}
	string data = "";
	string title = gtk_entry_get_text(GTK_ENTRY(noteRemtxtReminder));
	title = trim(title);
	// check that a reminder name was actually entered
	if (title == "") {
		message_box("You must enter a reminder name!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
		gtk_entry_set_text(GTK_ENTRY(noteRemtxtReminder), "");
		gtk_widget_grab_focus(noteRemtxtReminder);
		return;
	}
	// check that the reminder doesn't already exist
	string temp_name = to_lowercase(title);
	for (unsigned int x = 0; x < reminders.size(); x++) {
		string comp = trim(to_lowercase(reminders[x].title));
		if (temp_name == comp) {
			// the reminder already exists so don't add it
			message_box("That reminder already exists!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
			gtk_widget_grab_focus(noteRemtxtReminder);
			return;
		}
	}

	reminder temp;
	// add the reminder title to the buffer
	temp.title = title;
	data += temp.title;
	data += "|";
	// combine all the date/time data and add it into the buffer
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(noteRemchkDate))) {
		time_t t = time(0);
		tm *curr = localtime(&t);
		// a reminder date/time has been set so collect the data and combine it together
		temp.date.tm_mday = gtk_combo_box_get_active(GTK_COMBO_BOX(noteRemcboDD)) + 1;
		data += gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(noteRemcboDD));
		data += "/";
		temp.date.tm_mon = gtk_combo_box_get_active(GTK_COMBO_BOX(noteRemcboMM));
		data += std::to_string(gtk_combo_box_get_active(GTK_COMBO_BOX(noteRemcboMM)) + 1);
		data += "/";
		temp.date.tm_year = curr->tm_year + gtk_combo_box_get_active(GTK_COMBO_BOX(noteRemcboYYYY));
		data += gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(noteRemcboYYYY));
		data += " ";
		temp.date.tm_hour = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(noteRemspinHH));
		data += std::to_string(gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(noteRemspinHH)));
		data += ":";
		temp.date.tm_min = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(noteRemspinMM));
		data += std::to_string(gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(noteRemspinMM)));
	} else {
		// there is no date/time set for a reminder, so just add '0'
		data += "null";
	}
	data += "|";
	// add the notes into the buffer
	GtkTextIter start, end;
	GtkTextBuffer* notesbuf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(noteRemtxtNotes));
	gtk_text_buffer_get_bounds(notesbuf, &start, &end);
	string buf = gtk_text_buffer_get_text(notesbuf, &start, &end, FALSE);
	if (buf == "") {
		data += "null";
	} else {
		data += buf;
	}
	temp.notes = buf;
	// add the ADD REMINDER instruction to the start
	data = "A" + data;

	// send the new reminder to the server
	if (send_data(data, true)) {
		message_box("The reminder could not be added on the server!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
		// add the reminder into the queue to be synchronised with the server
		mtxSync.lock();
		rem_queue.push(data);
		mtxSync.unlock();
	}

	// add an entry into the listbox for the reminder, just with the reminder name
	add_list_item(temp.title.c_str());
	// add the reminder into the vector
	mtxSync.lock();
	reminders.push_back(temp);
	mtxSync.unlock();
}

void edit_reminder(GtkWidget *widget) {
	// this edits and changes the current selected reminder and synchronises it with the server
	mtxSync.lock();
	string title = gtk_entry_get_text(GTK_ENTRY(noteRemtxtReminder));
	title = trim(title);
	// first check whether the client is in editing state
	if (!editing) {
		// check whether the reminder currently exists
		if (title == "" || reminders.size() == 0) {
			message_box("That reminder doesn't exist!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
			gtk_widget_grab_focus(noteRemtxtReminder);
			mtxSync.unlock();
			return;
		}
		title = to_lowercase(title);
		for (unsigned int x = 0; x < reminders.size(); x++) {
			string comp = trim(to_lowercase(reminders[x].title));
			if (title == comp) {
				// the reminder exists so set the reminder name to edit
				edit_rem = x;
				break;
			} else if (x == (reminders.size() - 1)) {
				// a reminder of that name doesn't exist
				message_box("That reminder doesn't exist!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
				gtk_widget_grab_focus(noteRemtxtReminder);
				mtxSync.unlock();
				return;
			}
		}
		gtk_button_set_label(GTK_BUTTON(noteRembtnEdit), "Save");
		gtk_widget_grab_focus(noteRemtxtReminder);
		editing = true;
		mtxSync.unlock();
		return;
	}

	// check whether the reminder name is blank
	if (title == "") {
		message_box("You can't leave the reminder name blank!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
		gtk_entry_set_text(GTK_ENTRY(noteRemtxtReminder), "");
		gtk_widget_grab_focus(noteRemtxtReminder);
		mtxSync.unlock();
		return;
	}

	// store the old reminder name so that it can be referred to by the server
	string old_name = reminders[edit_rem].title;

	// set the new data for the reminder
	string data = "";
	data += old_name + "|";
	reminders[edit_rem].title = title;
	data += reminders[edit_rem].title;
	data += "|";
	// combine all the date/time data and add it into the buffer
	if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(noteRemchkDate))) {
		time_t t = time(0);
		tm *curr = localtime(&t);
		// a reminder date/time has been set so collect the data and combine it together
		reminders[edit_rem].date.tm_mday = gtk_combo_box_get_active(GTK_COMBO_BOX(noteRemcboDD)) + 1;
		data += gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(noteRemcboDD));
		data += "/";
		reminders[edit_rem].date.tm_mon = gtk_combo_box_get_active(GTK_COMBO_BOX(noteRemcboMM));
		data += std::to_string(gtk_combo_box_get_active(GTK_COMBO_BOX(noteRemcboMM)) + 1);
		data += "/";
		reminders[edit_rem].date.tm_year = curr->tm_year + gtk_combo_box_get_active(GTK_COMBO_BOX(noteRemcboYYYY));
		data += gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(noteRemcboYYYY));
		data += " ";
		reminders[edit_rem].date.tm_hour = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(noteRemspinHH));
		data += std::to_string(gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(noteRemspinHH)));
		data += ":";
		reminders[edit_rem].date.tm_min = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(noteRemspinMM));
		data += std::to_string(gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(noteRemspinMM)));
	} else {
		// there is no date/time set for a reminder, so just add 'null'
		data += "null";
		reminders[edit_rem].date = tm();
	}
	data += "|";
	// add the notes into the buffer
	GtkTextIter start, end;
	GtkTextBuffer* notesbuf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(noteRemtxtNotes));
	gtk_text_buffer_get_bounds(notesbuf, &start, &end);
	string buf = gtk_text_buffer_get_text(notesbuf, &start, &end, FALSE);
	if (trim(buf) == "") {
		data += "null";
	} else {
		data += buf;
	}
	reminders[edit_rem].notes = buf;
	// add the EDIT REMINDER instruction to the start
	data = "E" + data;

	// send the reminder to the server
	if (send_data(data, true)) {
		message_box("The reminder could not be changed on the server!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
		// add the reminder into the queue to be synchronised with the server
		rem_queue.push(data);
	}

	// clear all the items in the listbox and add them all again
	gtk_tree_store_clear(GTK_TREE_STORE(listboxstore));
	for (unsigned int x = 0; x < reminders.size(); x++) {
		add_list_item(reminders[x].title.c_str());
	}

	gtk_button_set_label(GTK_BUTTON(noteRembtnEdit), "Edit");
	editing = false;
	edit_rem = -1;
	mtxSync.unlock();
}

void delete_reminder(GtkWidget *widget, GdkEventKey *event) {
	if (event->keyval == GDK_KEY_Delete) {
		if (editing) {
			message_box("You are currently editing a reminder!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
			return;
		}
		// the delete key was pressed so delete the selected reminder
		string name;
		GtkTreeSelection *selection;
		GtkTreeModel *model;
		GtkTreeIter iter;

		selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(listbox));
		if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
			char *buf;
			gtk_tree_model_get(model, &iter, 0, &buf, -1);
			// the name of the selected reminder in the tree view has been retrieved
			name = buf;
			if (message_box(string("Delete the reminder \"" + name + "\"?").c_str(), "Confirm", GTK_BUTTONS_YES_NO, GTK_MESSAGE_QUESTION) != GTK_RESPONSE_YES) {
				// they chose not to delete that reminder so cancel the operation
				return;
			}
			mtxSync.lock();
			// search for the reminder in the vector and delete it
			for (unsigned int x = 0; x < reminders.size(); x++) {
				if (name == reminders[x].title) {
					// the reminder exists so delete that reminder
					reminders.erase(reminders.begin() + x);
					// then clear the listbox and then populate it again
					gtk_tree_store_clear(GTK_TREE_STORE(listboxstore));
					for (unsigned int x = 0; x < reminders.size(); x++) {
						add_list_item(reminders[x].title.c_str());
					}
					// finally set the widget values back to their defaults
					time_t t = time(0);
					tm *curr = localtime(&t);
					gtk_entry_set_text(GTK_ENTRY(noteRemtxtReminder), "");
					gtk_combo_box_set_active(GTK_COMBO_BOX(noteRemcboDD), curr->tm_mday - 1);
					gtk_combo_box_set_active(GTK_COMBO_BOX(noteRemcboMM), curr->tm_mon);
					gtk_combo_box_set_active(GTK_COMBO_BOX(noteRemcboYYYY), 0);
					gtk_spin_button_set_value(GTK_SPIN_BUTTON(noteRemspinHH), 0);
					gtk_spin_button_set_value(GTK_SPIN_BUTTON(noteRemspinMM), 0);
					gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(noteRemchkDate), false);
					toggle_date_time(0);
					GtkTextBuffer* notesbuf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(noteRemtxtNotes));
					gtk_text_buffer_set_text(notesbuf, "", 0);
					break;
				} else if (x == (reminders.size() - 1)) {
					// a reminder of that name doesn't exist
					message_box("That reminder doesn't exist!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
					gtk_widget_grab_focus(noteRemtxtReminder);
					mtxSync.unlock();
					return;
				}
			}
			name = "D" + name;
			// send the reminder to the server
			if (send_data(name, true)) {
				message_box("The reminder could not be deleted on the server!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
				// add the reminder into the queue to be synchronised with the server
				rem_queue.push(name);
			}
			mtxSync.unlock();
		} else {
			message_box("The reminder could not be retrieved!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
		}
	}
}

void add_list_item(const char *text) {
	// adds a new item onto the end of the listbox
	GtkTreeIter iter;
	gtk_tree_store_append(GTK_TREE_STORE(listboxstore), &iter, 0);
	gtk_tree_store_set(GTK_TREE_STORE(listboxstore), &iter, 0, text, -1);
	gtk_tree_view_set_model(GTK_TREE_VIEW(listbox), GTK_TREE_MODEL(listboxstore));
}

void reminder_selected(GtkWidget *widget) {
	// determine the index of the selected item

	mtxSync.lock();

	GtkTreeSelection *selection;
	GtkTreeModel *model;
	GtkTreeIter iter;

	selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(listbox));
	if (gtk_tree_selection_get_selected(selection, &model, &iter)) {
		char *name;
		gtk_tree_model_get(model, &iter, 0, &name, -1);

		// the name of the selected reminder in the tree view has been retrieved
		// use that name to search for that reminder in the vector in case there is an index mismatch
		// after the details of the reminder have been retrieved the data is put into the corresponding widgets
		unsigned int x = 0;
		for (; x < reminders.size(); x++) {
			if (reminders[x].title == name) {
				// the correct reminder vector element has been found
				break;
			} else if (x == (reminders.size() - 1)) {
				// CRITICAL ERROR - the reminder didn't exist in the vector
				message_box("A critical error has occurred! A data mismatch was encountered.", "Critical error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
				gtk_main_quit();
				mtxSync.unlock();
				return;
			}
		}
		// set the reminder title
		gtk_entry_set_text(GTK_ENTRY(noteRemtxtReminder), reminders[x].title.c_str());
		// set the date/time if one has been set
		time_t t = time(0);
		tm *curr = localtime(&t);
		if (reminders[x].date.tm_year < curr->tm_year) {
			// no date/time has been set
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(noteRemchkDate), false);
		} else {
			// a date/time has been set
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(noteRemchkDate), true);
			// put the data into the widgets
			gtk_combo_box_set_active(GTK_COMBO_BOX(noteRemcboDD), reminders[x].date.tm_mday - 1);
			gtk_combo_box_set_active(GTK_COMBO_BOX(noteRemcboMM), reminders[x].date.tm_mon);
			gtk_combo_box_set_active(GTK_COMBO_BOX(noteRemcboYYYY), reminders[x].date.tm_year - curr->tm_year);
			gtk_spin_button_set_value(GTK_SPIN_BUTTON(noteRemspinHH), reminders[x].date.tm_hour);
			gtk_spin_button_set_value(GTK_SPIN_BUTTON(noteRemspinMM), reminders[x].date.tm_min);
		}
		toggle_date_time(0);

		GtkTextBuffer* notesbuf = gtk_text_view_get_buffer(GTK_TEXT_VIEW(noteRemtxtNotes));
		gtk_text_buffer_set_text(notesbuf, reminders[x].notes.c_str(), reminders[x].notes.length());
	} else {
		message_box("The reminder could not be retrieved!", "Error!", GTK_BUTTONS_OK, GTK_MESSAGE_ERROR);
	}
	mtxSync.unlock();
}

void set_server(GtkWidget *widget, gpointer data_raw) {
	// this is called when the server is changed on the options page
	if (data_raw) {
		// set the new address to the one sent as a parameter
		string data = (char*)data_raw;
		gtk_entry_set_text(GTK_ENTRY(noteOpttxtServer), data.c_str());
		address = data;
	} else {
		// get the new address from the entry box and set that as the new one
		address = gtk_entry_get_text(GTK_ENTRY(noteOpttxtServer));
	}
	get_ip(address);
	gtk_label_set_text(GTK_LABEL(noteOptlblIP), string(string("IP Address:  ") + ip).c_str());
	message_box(string("The server has now been set to \"" + address + "\".").c_str(), "Server set", GTK_BUTTONS_OK, GTK_MESSAGE_INFO);
	write_config();
}

void get_ip(string location) {
	// returns the IP address of the address given to the function
	hostent *he = gethostbyname(address.c_str());
	if (!he) {
		// the pointer was invalid therefore the IP address of the hostname could not be retrieved
		// return a null IP address
		strcpy(ip, "0.0.0.0");
	}
	in_addr **addr_list = (struct in_addr**)he->h_addr_list;
	strcpy(ip, inet_ntoa(*addr_list[0]));
}

void about_info(GtkWidget *widget) {
	// show the 'about' information about the program
#ifdef _WIN32
	message_box("Copyright (c) Tom Albans 2015, All Rights Reserved\n\nWritten in C++.", "About", GTK_BUTTONS_OK, GTK_MESSAGE_INFO);
#else
	message_box("Copyright © Tom Albans 2015-2016, All Rights Reserved\n\nWritten in C++.", "About", GTK_BUTTONS_OK, GTK_MESSAGE_INFO);
#endif
}

int message_box(const char *text, const char *title, GtkButtonsType buttons, GtkMessageType icon) {
	GtkWidget* dialog = gtk_message_dialog_new(0, GTK_DIALOG_DESTROY_WITH_PARENT, icon, buttons, text, "");
	gtk_window_set_title(GTK_WINDOW(dialog), title);
	int res = gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
	return res;
}

void exit_client(GtkWidget *widget, gpointer data) {
	if (gtk_status_icon_is_embedded(trayicon)) {
		// the notification icon exists in their system tray
		int res;
		string ask = "";
		if (data != nullptr) {
            ask = (char*)data;
		}
		if (background && ask != "ask") {
            // just minimise straight into the tray
            gtk_widget_hide(window);
		} else {
            if ((res = message_box("Do you want to run in the background? Or press No to quit.", "Confirm", GTK_BUTTONS_YES_NO, GTK_MESSAGE_QUESTION)) == GTK_RESPONSE_YES) {
                // they wish to run the program in the background
                gtk_widget_hide(window);
			} else if (res == GTK_RESPONSE_NO) {
                // they wish to close the program
                gtk_main_quit();
			}
		}
	} else {
		// the notification icon doesn't exist on their desktop
		if (message_box("Are you sure you want to quit?", "Confirm", GTK_BUTTONS_YES_NO, GTK_MESSAGE_QUESTION) == GTK_RESPONSE_YES) {
			gtk_main_quit();
		}
	}
}

bool get_lock() {
#ifdef _WIN32
    HANDLE hMutex = OpenMutex(MUTEX_ALL_ACCESS, 0, "TomRemindersClient");

    if (!hMutex) {
        // the mutex doesn't exist so create it
        hMutex = CreateMutex(0, 0, "TomRemindersClient");
        return true;
    } else {
        // the mutex exists so an instance is already running
        return false;
    }
#else
    struct flock fl;

    fl.l_type = F_WRLCK;
    fl.l_whence = SEEK_SET;
    fl.l_start = 0;
    fl.l_len = 1;

    int fdlock = open("/tmp/reminders.pid", O_WRONLY|O_CREAT, 0666);
    if (fdlock == -1) {
        return 0;
    }

    if (fcntl(fdlock, F_SETLK, &fl) == -1) {
        return 0;
    }

    return 1;
#endif
}

string to_lowercase(string str) {
	for (unsigned int i = 0; i < str.length(); i++) {
		str[i] = str[i] | ' '; // Convert each character to lowercase
	}
	return str;
}

string trim(string str) {
	if (str.empty()) {
		str = "";
	} else {
		unsigned int first = str.find_first_not_of(' ');
		unsigned int last = str.find_last_not_of(' ');
		if (first < str.length() && last < str.length()) {
			// trim down the spaces from the string
			str = str.substr(first, last - first + 1);
		} else {
			// the entire string is just spaces or empty
			str = "";
		}
	}
	return str;
}

int to_int(string str) {
	// function that converts a number into an integer, if it fails it returns 0
	if (str.empty()) {
		// the string was empty so there is an error
		return 0;
	}

	bool negative = false;
	// check whether the number is negative
	if (str[0] == '-') {
		// the number is negative so convert the number as if it were positive and return its negative equivalent
		// cut down the string first and remove the negative sign
		str = str.substr(1, str.length() - 1);
		negative = true;
	}

	int num = 0;
	unsigned int power = str.length() - 1; // the number required to put 10 to the power of to get that digit
	for (unsigned int x = 0; x < str.length(); x++) {
		int am = (isascii(str[x]) ? 48 : 30);
		int temp = (int)str[x] - am;
		if (temp < 0 || temp > 9) {
			// the character was not a number so the whole number is invalid, return 0
			return 0;
		}
		// add onto the cumulative number variable, the current digit multiplied by 10 to the power of the power variable
		// also checking for any integer overflows that may occur, if one occurs then return 0
		num += (temp * (int)pow(10, power));
		if (num < 0) {
			// there was an integer overflow, so return 0
			return 0;
		}
		power--; // reduce the power
	}
	if (negative) {
		// convert the number into a negative
		num *= -1;
	}
	return num;
}

#ifdef _WIN32
void close(int socket) {
	// this is a windows only function
	closesocket(socket);
}
#endif
