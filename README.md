# Using Git #
Instructions on how to use Git in the Terminal to access and commit to this repository.

## Setting up the repository ##
1. Navigate to the directory you wish to (locally) store the repository

2. Run the command:  *git init*

3. To add a remote repository run:  *git remote add origin https://<username>@bitbucket.org/<address>.git*

4. To retrieve all the data from the repository run:  *git pull -u origin master*

## Using the repository ##
1. To make changes with a commit you first need to add the files you want to commit.  You can add all changed files by running:  *git add --all*

2. To make a new commit with a comment run:  *git commit -m "Enter comment"*

3. To then sync the changes with the online repository and add the commit run:  *git push -u origin master*

## Keywords ##
* '*origin*' is the location of the git repository

* '*master*' is the name of the branch