/*
Copyright (c) 2015-2016, Tom Albans <albans.thomas@gmail.com>

This file is part of Trippy.

Trippy is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Trippy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Trippy.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "trippy.h"

static output_class out;
static const string rem_path = "reminders.txt";

struct reminder {
    string title;
    string date;
    string notes;
};

static vector<reminder> rem;

int reminders() {

    // read in the reminders file and add any currently existing reminders into the vector
    {
        ifstream input(rem_path);
        vector<string> data;
        // if the file could be opened then read the data into the vector
        if (input) {
            while (!input.eof()) {
                string temp;
                getline(input, temp);
                data.push_back(temp);
            }
            // loop through the vector until an opening bracket is found
            for (unsigned int x = 0; x < data.size(); x++) {
                reminder temp;
                if (data[x] == "{") {
                    // the first line is the reminder title
                    x++;
                    temp.title = data[x];
                    // the second line is the date/time
                    x++;
                    temp.date = data[x];
                    // the rest of the lines up until the close bracket are notes
                    x++;
                    string notes = "";
                    while (true) {
                        if (data[x] == "}") {
                            // the final line of the reminder has been reached so break
                            break;
                        }
                        // if the last line of the notes is reached then don'd add any trailing new lines
                        if (data[x + 1] == "}") {
                            notes += data[x];
                        } else {
                            notes += data[x] + "\n";
                        }
                        x++;
                    }
                    temp.notes = notes;
                    // add the reminder into the vector, then check for another reminder
                    rem.push_back(temp);
                }
            }
        }
        input.close();
    }

    // create a socket to listen for server requests

    int listenfd = 0, connfd = 0;

    sockaddr_in serv_addr;
    int numrv;

    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    if (!listenfd) {
        out << string("Reminders[" + to_string(rem_port) + "]: Failure to retrieve socket!\n").c_str();
        return -1;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(rem_port);

    bind(listenfd, (sockaddr*)&serv_addr, sizeof(serv_addr));

    if (listen(listenfd, 20) == -1) {
        out << string("Reminders[" + to_string(rem_port) + "]: Failed to listen on port!\n").c_str();
        close(listenfd);
        return -1;
    }

    out << string("Reminders[" + to_string(rem_port) + "]: Server is running\n").c_str();


    char *recBuf = new char[4097];
    memset(recBuf, 0, 4097);

    // accept and respond to any server requests
    while (true) {

        string data;

        int bytecount = 0;

        connfd = accept(listenfd, (sockaddr*)0, 0); // accept awaiting request

        if ((bytecount = recv(connfd, recBuf, 4096, 0)) > 0) {
            data = recBuf;
            memset(recBuf, 0, 4097); // clear the receiving buffer for the next operation
        }

        // if the SYNC instruction was then send all the reminders stored on the server to the client
        if (data == "SYNC") {
            // call the synchronise function passing the communication socket
            int bytecount = 0;
            for (unsigned int x = 0; x < rem.size(); x++) {
                string data = rem[x].title + "|" + rem[x].date + "|" + rem[x].notes + (char)30;
                if (send_all(connfd, data.c_str(), data.length())) {
                    out << "The reminder couldn't be sent.\n";
                    break;
                }
            }
            close(connfd);
            continue;
        }
        close(connfd);

        // the data has been received from the client and placed into the data string
        // the first character is the instruction to perform: ADD, DELETE, EDIT
        string instruction = "";
        instruction += (char)data[0];
        data.erase(0, 1);

        if (instruction == "A") {
            // the ADD REMINDER instruction was received
            // split the data received up into the 3 separate sections, the separator is the pipeline character '|'
            reminder temp_rem;
            int x = 0;

            while (true) {
                string temp;
                int pos = data.find_first_of('|');
                if (x == 2 || pos < 0 || pos >= data.length()) {
                    // both the reminder title and the date have been retrieved OR another pipeline separator '|' couldn't be found
                    temp_rem.notes = data;
                    break;
                }
                temp = data.substr(0, pos);
                if (data.length() > (pos + 1)) {
                    // cut down the string to the next section of the input
                    data.erase(0, pos + 1);
                }
                if (x == 0) {
                    temp_rem.title = temp;
                } else if (x == 1) {
                    temp_rem.date = temp;
                }
                x++;
            }
            rem.push_back(temp_rem);
            out << string("Reminders[" + to_string(rem_port) + "]: Reminder accepted\n").c_str();
        } else if (instruction == "D") {
            // the DELETE REMINDER instruction was received
            // the reminder to delete will be determined by the title received from the client
            string dat = trim(to_lowercase(data));
            for (unsigned int x = 0; x < rem.size(); x++) {
                // search for the reminder name until it is found, then delete it
                // convert both subjects to lowercase before comparison, the reminder title's are not case-sensitive
                string vect = trim(to_lowercase(rem[x].title));
                if (vect == dat) {
                    // the reminder has been found in the vector
                    // remove that reminder from the vector
                    rem.erase(rem.begin() + x);
                    break;
                }
            }
            out << string("Reminders[" + to_string(rem_port) + "]: Reminder deleted\n").c_str();
        } else if (instruction == "E") {
            // the EDIT REMINDER instruction was received
            reminder temp_rem;
            string srcRem;
            int x = 0;

            while (true) {
                if (x >= 3) {
                    // both the reminder title and the date have been retrieved OR another pipeline separator '|' couldn't be found
                    temp_rem.notes = data;
                    break;
                }
                string temp;
                int pos = data.find('|');
                if (pos >= 0 && pos < data.length()) {
                    temp = data.substr(0, pos);
                    data.erase(0, pos + 1);
                } else {
                    x = 3;
                    continue;
                }

                if (x == 0) {
                    srcRem = temp;
                } else if (x == 1) {
                    temp_rem.title = temp;
                } else if (x == 2) {
                    temp_rem.date = temp;
                }
                x++;
            }

            // search for the reminder and apply the changes to that element
            srcRem = trim(to_lowercase(srcRem));
            for (unsigned int i = 0; i < rem.size(); i++) {
                // search for the reminder name until it is found, then apply the changes
                string vect = trim(to_lowercase(rem[i].title));
                if (vect == srcRem) {
                    // the reminder has been found in the vector
                    // apply the changes
                    rem[i].title = temp_rem.title;
                    rem[i].date = temp_rem.date;
                    rem[i].notes = temp_rem.notes;
                    break;
                }
            }
            out << string("Reminders[" + to_string(rem_port) + "]: Reminder edited\n").c_str();
        } else {
            // an invalid instruction was received
            out << string("Reminders[" + to_string(rem_port) + "]: An invalid instruction was received!\n").c_str();
        }

        // write all the reminders (including changes) in the vector to the file
        ofstream file(rem_path);
        for (unsigned int x = 0; x < rem.size(); x++) {
            file << "{\n";
            file << rem[x].title << "\n";
            file << rem[x].date << "\n";
            file << rem[x].notes << "\n";
            file << "}\n";
        }
        file.close();
    }

    delete[] recBuf;

    return 0;
}

int send_all(int sockfd, const char *buf, int len) {
    int total = 0; // how many bytes we've sent
    int bytesleft = len; // how many we have left to send
    int n = 0;
    while(total < len) {
       n = send(sockfd, buf + total, bytesleft, 0);
       if (n == -1) {
           /* print/log error details */
           break;
       }
       total += n;
       bytesleft -= n;
    }
    len = total; // return number actually sent here
    return n == 1 ? 1 : 0; // return -1 on failure, 0 on success
}
























