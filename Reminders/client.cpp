/*
Copyright (c) 2015-2016, Tom Albans <albans.thomas@gmail.com>

This file is part of Trippy.

Trippy is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Trippy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Trippy.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "reminders.h"



void time_passed() {
	// this function runs in a separate thread and checks if any reminder times have passed
	while (true) {
		pause(2000); // check if any reminders have passed every 2 seconds
					 // first get the current time and the reminder time to compare it to
		time_t current = time(0);
		for (unsigned int x = 0; x < reminders.size(); x++) {
			if (reminders[x].date.tm_year <= 0) {
				// either no reminder was set or the user was already reminded of this reminder, skip it
				continue;
			}
			time_t comp = mktime(&reminders[x].date);
			double diff = difftime(current, comp);
			// if the result is positive then the reminder time has passed
			// the current time in seconds is greater than the reminder time in seconds
			if (diff > 0) {
				// the reminder has passed
				gdk_threads_add_idle(reminder_passed, (void*)(reminders[x].title.c_str()));
				mtxSync.lock();
				reminders[x].date = tm();
				mtxSync.unlock();
			}
		}
	}
}

int reminder_passed(gpointer data) {
	message_box((char*)data, "Reminder!", GTK_BUTTONS_OK, GTK_MESSAGE_INFO);
	return G_SOURCE_REMOVE;
}



void pause(int duration) {
	// sleep for the designated amount of time in milliseconds
	std::this_thread::sleep_for(std::chrono::milliseconds(duration));
	return;
}
