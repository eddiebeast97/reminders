/*
Copyright (c) 2015-2016, Tom Albans <albans.thomas@gmail.com>

This file is part of Trippy.

Trippy is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Trippy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Trippy.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifdef _WIN32
#define _CRT_SECURE_NO_WARNINGS
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <winsock2.h>
#include <Ws2tcpip.h>
#else
#include <sys/file.h>
#include <unistd.h>
#endif
#include <gtk/gtk.h>
#include <iostream>
#include <fstream>
#include <thread>
#include <string>
#include <vector>
#include <mutex>
#include <cstring>
#include <ctime>
#include <cmath>
using std::ofstream;
using std::thread;
using std::string;
using std::vector;
using std::mutex;
using std::cout;
using std::endl;
using std::cin;
using std::ifstream;

void toggle_date_time(GtkWidget*);
void date_changed(GtkWidget*);
void tray_click(GtkWidget*);
void tray_right_click(GtkWidget*);
void text_entry_activate(GtkWidget*);
void add_reminder(GtkWidget *widget);
void edit_reminder(GtkWidget*);
void delete_reminder(GtkWidget*, GdkEventKey*);
void add_list_item(const char*);
void reminder_selected(GtkWidget*);
void about_info(GtkWidget*);
int message_box(const char*, const char*, GtkButtonsType, GtkMessageType);
void exit_client(GtkWidget*, gpointer);
bool get_lock();
string to_lowercase(string);
string trim(string);
int to_int(string);
void read_rem();
void write_rem();
#ifdef _WIN32
void close(int);
#endif

void time_passed();
int reminder_passed(gpointer);
void pause(int);

struct reminder {
	string title; // UNIQUE - the title of the reminder
	tm date; // time structure for the reminder date & time
	string notes; // the entirety of the notes text view, includes newline, '\n', characters

	reminder() {
		title = "";
		date = tm();
		notes = "";
	}

};

extern vector<reminder> reminders;
extern bool background;
extern mutex mtxSync;
extern const string rem_path;

extern GtkWidget *window;
extern GtkTreeStore *listboxstore;
extern GtkWidget *noteRemtxtReminder;
extern GtkWidget *noteRemchkDate;
extern GtkWidget *noteRemcboDD;
extern GtkWidget *noteRemcboMM;
extern GtkWidget *noteRemcboYYYY;
extern GtkWidget *noteRemspinHH;
extern GtkWidget *noteRemspinMM;
extern GtkWidget *noteRemtxtNotes;
