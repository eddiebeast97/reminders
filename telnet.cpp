/*
Copyright (c) 2015-2016, Tom Albans <albans.thomas@gmail.com>

This file is part of Trippy.

Trippy is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Trippy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Trippy.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "trippy.h"

static const unsigned int connections = 5;

static output_class out;
static int listenfd = 0;
static int i = 0;

static string clear = "\033[2J";

int telnet() {

    sockaddr_in serv_addr;

    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    if (!listenfd) {
        out << string("Telnet[" + to_string(tel_port) + "]: Failure to retrieve socket!\n").c_str();
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(tel_port);

    bind(listenfd, (sockaddr*)&serv_addr, sizeof(serv_addr));

    if (listen(listenfd, connections) == -1) {
        out << string("Telnet[" + to_string(tel_port) + "]: Failed to listen!\n").c_str();
        return -1;
    }

    out << string("Telnet[" + to_string(tel_port) + "]: Server is running\n").c_str();


    thread req[connections];

    while (true) {

        int sockfd = accept(listenfd, (sockaddr*)0, 0); // accept an awaiting request
        if (!sockfd || i >= 10) {
            close(sockfd);
            continue;
        }
        req[i] = thread(telnet_request, sockfd);
        req[i].detach();
        i++;

        out << string("Telnet[" + to_string(tel_port) + "]: Connection accepted\n").c_str();
    }

    return 0;
}

void telnet_request(int sock) {

    // the raw character array is where the data is read into
    // once the data has been retrieved it is placed into the string for easier manipulation and usage
    // this string acts as both an output before sending it and an input for retrieving data from the client
    char *recBuf = new char[4097];
    string data = "hello\n";
    // carry on processing the request until the connection is closed
    while (true) {

        int bytecount = 0;

        // send the output/result to the client that is connected to the telnet server
        if ((bytecount = send(sock, data.c_str(), data.length(), 0)) == -1) {
            out << string("Telnet[" + to_string(tel_port) + "]: The data could not be sent!\n").c_str();
            break;
        }

        // get the input from the telnet client and process it in the server ready for the next output
        if ((bytecount = recv(sock, recBuf, 4096, 0)) > 0) {
            data = recBuf;
            memset(recBuf, 0, 4097); // clear the receiving buffer for the next operation
        } else {
            break;
        }
    }
    delete[] recBuf;
    // when the connection is closed it goes back to the beginning to process another request
    close(sock);
    sock = 0;
    i--;
    out << string("Telnet[" + to_string(tel_port) + "]: Connection accepted\n").c_str();
}