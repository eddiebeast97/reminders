/*
Copyright (c) 2015-2016, Tom Albans <albans.thomas@gmail.com>

This file is part of Trippy.

Trippy is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Trippy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Trippy.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <vector>
#include <thread>
#include <chrono>
#include <mutex>
using std::to_string;
using std::ifstream;
using std::ofstream;
using std::thread;
using std::string;
using std::vector;
using std::mutex;
using std::cout;
using std::endl;

void pause(int);
string to_lowercase(string);
string trim(string);

int reminders();
int send_all(int, const char*, int);
const unsigned int rem_port = 5050;

int telnet();
void telnet_request(int);
const unsigned int tel_port = 2323;


static mutex mtxOut;

class output_class {
private:

public:

    output_class& operator<<(const char* text) {
        mtxOut.lock();
        cout << text;
        mtxOut.unlock();
        return *this;
    }

};