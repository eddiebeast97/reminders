/*
Copyright (c) 2015-2016, Tom Albans <albans.thomas@gmail.com>

This file is part of Trippy.

Trippy is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Trippy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Trippy.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "trippy.h"

static thread rem;
static thread tel;

int main(int argc, char **argv) {

    // start the Reminders server thread
    rem = thread(reminders);

    // start the Telnet server thread
    tel = thread(telnet);

    // when all the servers have finished running then close down the program
    rem.join();
    tel.join();
    return 0;
}

void pause(int duration) {
	// sleep for the designated amount of time in milliseconds
	std::this_thread::sleep_for(std::chrono::milliseconds(duration));
	return;
}

string to_lowercase(string str) {
    for (unsigned int i = 0; i < str.length(); i++) {
        str[i] = str[i] | ' '; // Convert each character to lowercase
    }
    return str;
}

string trim(string str) {
    if (str.empty()) {
        str = "";
    } else {
        unsigned int first = str.find_first_not_of(' ');
        unsigned int last = str.find_last_not_of(' ');
        if (first < str.length() && last < str.length()) {
            // trim down the spaces from the string
            str = str.substr(first, last - first + 1);
        } else {
            // the entire string is just spaces or empty
            str = "";
        }
    }
    return str;
}